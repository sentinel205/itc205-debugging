import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class CannotReachBetLimit {
	// Class under test
	Player testPlayer;
	
	// Dependencies
	
	/**
	 * Execute before each test
	 */
	@Before
	public void before() {
		// Set the player balance to 5 (when the error occurs)
		testPlayer = new Player("George", 5);
	}
	
	/**
	 * Execute after each test
	 */
	@After
	public void after() {
		testPlayer = null;
	}
	
	/**
	 * Test that the limit checker method returns true when the remaining balance is equal to the
	 * bet
	 */
	@Test
	public void testBalanceExceedsLimitBy () {
		// Assert
		assertTrue(testPlayer.balanceExceedsLimitBy(5) == true);
	}
	
}

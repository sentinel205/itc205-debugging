import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class BetsAreOff {
	// Class under test
	Dice testDie;
	
	// Dependencies
	
	/**
	 * Execute before each test
	 */
	@Before
	public void before() {
		testDie = new Dice();
	}
	
	/**
	 * Execute after each test
	 */
	@After
	public void after() {
		testDie = null;
	}
	
	/**
	 * Test that the value of the die changes after being rolled
	 */
	@Test
	public void testDiceValueIsDifferentToRoll () {
		// Arrange
		DiceValue firstRoll = testDie.getValue();
		
		// Execute
		DiceValue secondRoll = testDie.roll();
		DiceValue thirdRoll = testDie.roll();
		
		// Assert
		assertTrue(!firstRoll.equals(secondRoll));
		assertTrue(!secondRoll.equals(thirdRoll));
	}
	
	@Test
	public void testDiceValueChangesAfterRoll () {
		// Arrange
		DiceValue firstRoll = testDie.getValue();
		
		// Execute
		DiceValue secondRoll = testDie.roll();
		DiceValue secondRollValue = testDie.getValue();
		DiceValue thirdRoll = testDie.roll();
		DiceValue thirdRollValue = testDie.getValue();
		
		// Assert
		assertTrue(secondRoll.equals(secondRollValue));
		assertTrue(thirdRoll.equals(thirdRollValue));
	}
}

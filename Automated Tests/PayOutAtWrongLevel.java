import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class PayOutAtWrongLevel {
	// Class under test
	Game testGame;
	
	// Dependencies (not mocked)
	Player testPlayer;
	
	// Dependencies (to be mocked)
	Dice testDie1;
	Dice testDie2;
	Dice testDie3;
	
	/**
	 * Execute before each test
	 */
	@Before
	public void before() {
		testDie1 = mock(Dice.class);
		testDie2 = mock(Dice.class);
		testDie3 = mock(Dice.class);
		testGame = new Game(testDie1, testDie2, testDie3);
		testPlayer = new Player("George", 100);
		
		when(testDie1.roll()).thenReturn(DiceValue.ANCHOR);
		when(testDie2.roll()).thenReturn(DiceValue.DIAMOND);
		when(testDie3.roll()).thenReturn(DiceValue.CROWN);
		
		when(testDie1.getValue()).thenReturn(DiceValue.ANCHOR);
		when(testDie2.getValue()).thenReturn(DiceValue.DIAMOND);
		when(testDie3.getValue()).thenReturn(DiceValue.CROWN);
	}
	
	/**
	 * Execute after each test
	 */
	@After
	public void after() {
		testGame = null;
		reset(testDie1);
		reset(testDie2);
		reset(testDie3);
		testPlayer = null;
	}
	
	@Test
	public void testPlayerGivenWrongAmountOn1to1Win () {
		System.out.println("***testPlayerGivenWrongAmountOn1to1Win***");
		
		DiceValue val4 = DiceValue.ANCHOR;
		
		System.out.println("Player pick: " + val4);
		
		// Execute
		int winnings = testGame.playRound(testPlayer, val4, 5);
		System.out.println("Dice values: " + testDie1.getValue() + " "
				+ testDie2.getValue() + " " + testDie3.getValue());
		System.out.println("Winnings: " + winnings);
		
		// Assert
		assertTrue(winnings > 5);
	}
}
